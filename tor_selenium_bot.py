# imports
from selenium import webdriver
# Tor process managment
import subprocess
import stem.socket
import stem.connection
import stem.process
from stem.control import Controller
from stem import Signal
# Tor random nodes dirs
import os
import shutil
# delay
import time
import random

########
#CONFIG#
########

# dyanamic Tor setup and controller
# this is recommended as this lets you run concurrent instances of this script
random_control_port = True
random_socks_port = True
rp_start = 49152
rp_end = 65535
# make sure this folder is ONLY shared with other instances of this script
# or other self cleaning emphemeral proccesses
parent_data_dir = "/tmp/tor_selenium_bot"

# static tor setup
tor_path = "/usr/bin/tor"
torrc_path = "torrc"
# put the same config as the torrc used
socks_port = 9150
control_port = 9151
controller_password = 'password'

# hit config
hit_url = "https://duckduckgo.com"
hit_count = 1
# sleep time
st_start = 60 # 1min
st_end = 60*60 # 60mins
random_sleep = True

############
#CONFIG END#
############

# globals vars
control_socket = None
tor_process = None
data_dir = None

# a "hit" is one iteration of a cycle, IPs are changed between hits
def hit():
    browser = webdriver.Firefox(getFirefoxProfile())
    browser.get(hit_url)
    browser.save_screenshot("screenshot.png")
    browser.close()

def run_hits():
    for h in range(0, hit_count):
        print("Running Hit {}/{}".format(h + 1, hit_count))
        hit()
        if (h != hit_count - 1):
            renew_tor_ip()
            wait = hang_until_connect()
            if (random_sleep):
                st = random.randint(st_start, st_end)
                print("Sleeping for {}s".format(st))
                time.sleep(st)

def getFirefoxProfile():
    profile = webdriver.FirefoxProfile()

    # emphemeral instances
    profile.set_preference( "places.history.enabled", False)
    profile.set_preference( "privacy.clearOnShutdown.offlineApps", True)
    profile.set_preference( "privacy.clearOnShutdown.passwords", True)
    profile.set_preference( "privacy.clearOnShutdown.siteSettings", True)
    profile.set_preference( "privacy.sanitize.sanitizeOnShutdown", True)
    profile.set_preference( "signon.rememberSignons", False)
    profile.set_preference( "network.cookie.lifetimePolicy", 2)

    # extra privacy stuff
    profile.set_preference( "network.dns.disablePrefetch", True)
    profile.set_preference( "network.http.sendRefererHeader", 0)
    profile.set_preference( "javascript.enabled", False)

    # disable images for faster load
    profile.set_preference( "permissions.default.image", 2)

    # socks5 proxy setup
    profile.set_preference( "network.proxy.type", 1)
    profile.set_preference( "network.proxy.socks_version", 5)
    profile.set_preference( "network.proxy.socks", '127.0.0.1')
    profile.set_preference( "network.proxy.socks_port", int(socks_port))
    profile.set_preference( "network.proxy.socks_remote_dns", True)

    return profile

def init_tor_process():
    global tor_process
    global control_port
    global data_dir

    tor_connected = False
    # old subprocess logic which stem returns anyway
    # use this if you cannot get stem to work, use kill -HUP to renew IP
    '''
    try:
        tor_process = subprocess.Popen([tor_path, "-f", torrc_path])
        tor_connected = True
    except OSError as e:
        print(e)

    '''
    try:
        sp = str(socks_port)
        cp = str(control_port)
        data_dir = parent_data_dir + "/S{}C{}".format(sp, cp)
        tor_process = stem.process.launch_tor_with_config(
          config = {
            'SOCKSPort': sp,
            'ControlPort': cp,
            'DataDirectory': data_dir,
          },
        )
        tor_connected = True
        print("Tor process connected")
    except OSError as e:
        print("OSError: {}".format(e))
    except Exception as e:
        print(e)

    return tor_connected

def hang_until_connect():
    global control_socket

    try_count = 3
    c_try = 0
    connection = False

    while(not connection and c_try < try_count):
        try:
            control_socket = stem.socket.ControlPort(port = control_port)
            # check that everything is good with your tor_process by checking bootstrap status
            connection = True
        except stem.SocketError as e:
            print ('Unable to connect to port {}'.format(control_port))
            print ("Attempt: {} out of {}".format(c_try+1, try_count))
            print ("stem.SocketError: {}".format(e))
        except Exception as e:
            print(e)

        c_try += 1
        time.sleep(1)

    if (connection):
        try:
            stem.connection.authenticate(control_socket)
        except stem.connection.IncorrectSocketType:
            print ("stem.connection.IncorrectSocketType: {}".format(e))
        except stem.connection.MissingPassword:
            try:
                stem.connection.authenticate_password(control_socket, controller_password)
                print('Connection Obtained :)')
            except stem.connection.PasswordAuthFailed:
                print ("stem.connection.PasswordAuthFailed: {}".format(e))
        except stem.connection.AuthenticationFailure as e:
            print ("stem.connection.AuthenticationFailure: {}".format(e))
        except Exception as e:
            print(e)

    return connection

def renew_tor_ip():
    with Controller.from_port(port = control_port) as controller:
        controller.authenticate(password=controller_password)
        controller.signal(Signal.NEWNYM)

def create_temp_dir():
    if (not os.path.isdir(parent_data_dir)):
        try:
            os.makedirs(parent_data_dir)
        except FileExistsError as e:
            print("FileExistsError: {}".format(e))
        except Exception as e:
            print("Error in create_temp_dir(): {}".format(e))

def clean_temp_dir():
    # attempt to clean up own Tor instance
    shutil.rmtree(data_dir)
    # attempt to remove the parent_data_dir
    # this only works if it's empty aka when no more instances are running
    safermdir(parent_data_dir)

def safermdir(d):
    if (os.path.isdir(d)):
        try:
            print("Attempting to remove {}".format(d))
            os.rmdir(d)
        except OSError as e:
            print("OSError in safermdir() in removing: {}".format(d, e))
            print("If you have other instances running and sharing the same parent_data_dir then don't worrry :)")
            print("Otherwise check the dir along with your permissions then manuallly clean it if need be.")
        except Exception as e:
            print("Error in safermdir() in removing: {}".format(d, e))


if __name__ == "__main__":
    tor_connected = False
    connected = False

    if (random_socks_port):
        socks_port = random.randint(rp_start, rp_end)
        print("Random SOCKS port: {}".format(socks_port))

    if (random_control_port):
        control_port = random.randint(rp_start, rp_end)
        while(socks_port == control_port):
            control_port = random.randint(rp_start, rp_end)
        print("Random Control port: {}".format(control_port))

    create_temp_dir()

    # only try to connect to the ports if it's not using a random port
    if (not (random_socks_port or random_control_port)):
        connected = hang_until_connect()

    if (not connected):
        print("Trying to make a new Tor process")
        tor_connected = init_tor_process()

    connected = hang_until_connect()

    if (not tor_connected):
        print ("Tor is not connected!")
    if (not connected):
        print ("STEM Tor python lib is not connected!")

    if (connected and tor_connected):
        run_hits()

    if (tor_process != None):
        try:
            print("Killing Tor subprocess")
            tor_process.kill()
        except Exception as e:
           print(e)
 
    clean_temp_dir()

    print("Goodbye :)")
