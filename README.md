# Tor Selenium Bot
A simple to use and extend script for using Selenium FireFox with Tor traffic via local SOCKS5 proxy instances.
This might be the better approach for when you want to randomize your IP but cannot use the Tor Browser.

Also see https://github.com/webfp/tor-browser-selenium

# Dependencies
* [stem](https://stem.torproject.org/)
* [selenium](https://www.selenium.dev/)

# Warning and Notes
1) The default Tor session's data directory is "/tmp/tor_selenium_bot"
This is not a problem if you are a single user on a secure machine.
Otherwise change the parent directory to something private and not publicly assessable.

2) The local instances spawned by stem are unprotected from any passwords.

3) If the script terminates early, either due to a crash or user intervention (e.g. Ctrl+D) it probably won't
shutdown cleanly.
	a) Wipe the temp directory as it contains Tor keys and other sensitive information.
	b) close the child Tor subprocess. This will be eating up resources, clogging up ports, and more.

# Platform Support
It works for me on Arch Linux but it should be okay with any distro.

Other OSs should also be supported just change the paths. Open a ticket or message me if you need help :)

# Licence
Free for personal use. Anything else, contact me.
